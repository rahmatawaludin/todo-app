<?php
class TodoList extends Eloquent
{
	public function todoItems()
	{
		return $this->hasMany('TodoItem');
	}
}
