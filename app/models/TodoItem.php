<?php
class TodoItem extends Eloquent
{
	public function todo()
	{
		return $this->belongsTo('TodoList');
	}
}
