<?php
class TodoListTableSeeder extends Seeder {

    public function run()
    {
        DB::table('todo_lists')->delete();

       	$lists = ['Car', 'Shopping', 'School', 'Work', 'Job', 'Fruit'];
        $pem = User::where('email', 'awaludin@gmail.com')->firstOrFail();
        foreach ($lists as $list) {
          $newlist = new TodoList();
          $newlist->title = $list . " List";
          $newlist->user_id = $pem->id;
          $newlist->save();
        }

        $lists = ['Handphone', 'Tablet', 'Laptop', 'Mac'];
        $ciast = User::where('email', 'rahmat@gmail.com')->firstOrFail();
        foreach ($lists as $list) {
          $newlist = new TodoList();
          $newlist->title = $list . " List";
          $newlist->user_id = $ciast->id;
          $newlist->save();
        }
    }

}
