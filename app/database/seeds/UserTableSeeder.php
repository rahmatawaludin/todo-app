<?php
class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

       	$user1 = new User();
       	$user1->username = "rahmat";
       	$user1->email = "rahmat@gmail.com";
       	$user1->password = Hash::make('secret');
       	$user1->save();

       	$user2 = new User();
       	$user2->username = "awaludin";
       	$user2->email = "awaludin@gmail.com";
       	$user2->password = Hash::make('secret');
       	$user2->save();
    }

}
