<?php
class TodoItemTableSeeder extends Seeder {

    public function run()
    {
        DB::table('todo_items')->delete();

        $list = TodoList::where('title', 'Car List')->firstOrFail();
        $cars = ['Buy BMW', 'Buy Proton', 'Buy Ferrari'];
        foreach ($cars as $car) {
          $item = new TodoItem();
          $item->content = $car;
          $item->todo_list_id = $list->id;
          $item->save();
        }

    }

}
