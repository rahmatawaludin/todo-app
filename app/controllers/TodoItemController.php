<?php

class TodoItemController extends \BaseController {

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($todo)
	{
		$item = new TodoItem();
		$item->content = Input::get('content');
		$item->todo_list_id = $todo;
		$item->save();
		return Redirect::route('todos.show', $todo)->withMessages('Todo Item added!');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


}
