<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function login()
	{
		return View::make('home.login');
	}

	public function loginPost()
	{
		$email = Input::get('email');
		$password = Input::get('password');

		if (Auth::attempt(array('email' => $email, 'password' => $password)))
		{
		    return Redirect::intended('todos');
		} else {
			return Redirect::to('/login')->withNotices('Invalid credentials.. :(');
		}
	}

	public function logout()
	{
		Auth::logout();
		return Redirect::to('/')->withMessages('Successfully logout!');
	}

    public function signup()
    {
        return View::make('home.signup');
    }

    public function signupPost()
    {
        // Define rules
        $rules = [
            'username' => 'required|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed'
        ];

        // Validate input
        $validator = Validator::make(Input::all(), $rules);

        // check validity
        if ($validator->fails()) {
            return Redirect::to('/signup')->withErrors($validator)->withInput();
        }
        // creating user
        $user = new User();
        $user->username = Input::get('username');
        $user->email = Input::get('email');
        $user->password = Hash::make(Input::get('password'));
        $user->save();

        // redirect to login
        return Redirect::to('/login')->withMessages('Account created. You can login now!');
    }


}
