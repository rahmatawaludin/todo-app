<?php

class TodoController extends \BaseController {

	public function __construct()
	{
		$this->beforeFilter('csrf', ['on' => ['post', 'put', 'delete']]);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$todos = Auth::user()->todoLists;
		return View::make('todos.index')->withTodos($todos);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('todos.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// Define Rules
		$rules = ['title' => 'required|unique:todo_lists'];

		// Pass Input to rules
		$validator = Validator::make(Input::all(), $rules);

		// Test validity
		if ($validator->fails()) {
			return Redirect::route('todos.create')->withErrors($validator)->withInput();
		}

		$todo = new TodoList();
		$todo->title = Input::get('title');
        $todo->user_id = Auth::user()->id;
		$todo->save();
		return Redirect::route('todos.index')->withMessages('Todo List Created!');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$todo = TodoList::findOrFail($id);
		return View::make('todos.show')->withTodo($todo);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$todo = TodoList::findOrFail($id);

		if ($todo->user_id == Auth::user()->id) {
			return View::make('todos.edit')->withTodo($todo);
		}

		return Redirect::route('todos.index')->withNotices('You are not authorized!');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// Define Rules
		$rules = ['title' => 'required|unique:todo_lists'];

		// Pass Input to rules
		$validator = Validator::make(Input::all(), $rules);

		// Test validity
		if ($validator->fails()) {
			return Redirect::route('todos.edit', $id)->withErrors($validator)->withInput();
		}

		$todo = TodoList::findOrFail($id);
		$todo->title = Input::get('title');
        $todo->user_id = Auth::user()->id;
		$todo->save();
		return Redirect::route('todos.index')->withMessages('Todo List Updated!');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// Cari Todo
		$todo = TodoList::findOrFail($id);
		// Hapus
		$todo->delete();
		// Redirect
		return Redirect::route('todos.index')->withMessages('Todo has been deleted!');
	}

	public function printPDF($id)
	{
		$todo = TodoList::findOrFail($id);
		$data = ['todo'=>$todo];
		$pdf = PDF::loadView('pdf.todo', $data);
		return $pdf->download($todo->title.'.pdf');
	}

}
