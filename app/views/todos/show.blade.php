@extends('layouts.main')
@section('content')
	<h1> Viewing todo {{{ $todo->title }}}</h1>
	<ul id="todo-items">
		@foreach ($todo->todoItems as $item)
			<li>{{{ $item->content }}}</li>
		@endforeach
	</ul>


	{{ Form::open(['route'=>['todos.items.store', $todo->id], 'id'=>'addItem', 'class'=>'form-inline']) }}
		{{ Form::text('content', null, ['class'=>'form-control']) }}
		{{ Form::submit('Add', ['class'=>'btn btn-primary']) }}
	{{ Form::close() }}

@stop

@section('footer')
	<script>
		// wait for the DOM to be loaded
	    $(document).ready(function() {
	        // bind 'myForm' and provide a simple callback function
	        $('#addItem').ajaxForm(function() {
	        	// add new item
	        	var content = $('#addItem').children(':text[name=content]').val();
            	$('<li>'+content+'</li>').hide().appendTo('#todo-items').fadeIn('slow');
            	// clear input box
            	$('#addItem').children(':text[name=content]').val('');
	        });
	    });
	</script>
@stop
