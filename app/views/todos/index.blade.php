@extends('layouts.main')
@section('content')
	Viewing all todos
	@foreach ($todos as $todo)
		<h1>{{ link_to_route('todos.show', $todo->title, ['id'=>$todo->id])}}</h1>
		{{ link_to_route('todos.print', 'Print', ['id'=>$todo->id], ['class'=>'btn btn-success'])}}
		{{ link_to_route('todos.edit', 'Edit', ['id'=>$todo->id], ['class'=>'btn btn-warning'])}}
		{{ Form::model($todo, ['route'=>['todos.destroy', $todo->id], 'method'=>'delete', 'style'=>'display:inline-block;'] )}}
			{{ Form::submit('Delete', ['class'=>'btn btn-danger']) }}
		{{ Form::close() }}
	@endforeach
	<hr>
	{{ link_to_route('todos.create', 'New Todo', null, ['class'=>'btn btn-primary btn-lg']) }}
@stop
