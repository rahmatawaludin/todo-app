<div class="form-group">
	{{ Form::label('title', 'Todo Title')}}
	{{ Form::text('title', null, ['class'=>'form-control', 'placeholder'=>'Your list title']) }}
</div>
{{ $errors->first('title', '<div class="alert alert-danger">:message</div>') }}
	{{ Form::submit('Save', ['class'=>'btn btn-default']) }}
	{{ link_to_route('todos.index', 'Cancel', null, ['class'=>'btn btn-primary']) }}
