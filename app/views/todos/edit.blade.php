@extends('layouts.main')
@section('content')
	{{ Form::model($todo, array('route'=>['todos.update', $todo->id], 'method'=>'put')) }}
		@include('todos.partials._form')
	{{ Form::close() }}
@stop
