<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tudu App</title>

    <!-- Bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="{{ asset('css/bootstrap-theme.min.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body role="">
    <nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Tudu App</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <!-- navigation list here -->
          </ul>
          @if (Auth::check())
            {{-- display logout button --}}
            <ul class="nav navbar-nav navbar-right">
              <li><a href="/logout">Logout</a></li>
            </ul>
          @else
            {{-- display login buton --}}
            <ul class="nav navbar-nav navbar-right">
              <li><a href="/login">Login</a></li>
            </ul>
          @endif
      </div>
    </nav>

    <div class="container">

      @if (Session::has('messages'))
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert">
            <span>&times;</span>
            <span class="sr-only">Close</span>
          </button>
          {{ Session::get('messages')}}
        </div>
      @endif

      @if (Session::has('notices'))
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert">
            <span>&times;</span>
            <span class="sr-only">Close</span>
          </button>
          {{ Session::get('notices')}}
        </div>
      @endif

      @yield('content')
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    {{ HTML::script('js/jquery.min.js') }}

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    {{ HTML::script('js/bootstrap.min.js') }}

    {{ HTML::script('js/app.js') }}
    {{ HTML::script('js/jquery.form.min.js') }}
    @yield('footer')
  </body>
</html>
