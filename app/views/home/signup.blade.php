@extends('layouts.main')
@section('content')
	{{ Form::open(['url'=>'signup-post']) }}

		<div class="form-group">
			{{ Form::label('username', 'Username')}}
			{{ Form::text('username', null, ['class'=>'form-control', 'placeholder'=>'Your username']) }}
		</div>
		{{ $errors->first('username', '<div class="alert alert-danger">:message</div>') }}

		<div class="form-group">
			{{ Form::label('email', 'Email')}}
			{{ Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Your email']) }}
		</div>
		{{ $errors->first('email', '<div class="alert alert-danger">:message</div>') }}

		<div class="form-group">
			{{ Form::label('password', 'Password')}}
			{{ Form::password('password', ['class'=>'form-control', 'placeholder'=>'Your password']) }}
		</div>
		{{ $errors->first('password', '<div class="alert alert-danger">:message</div>') }}
		<div class="form-group">
			{{ Form::label('password_confirmation', 'Confirm Password')}}
			{{ Form::password('password_confirmation', ['class'=>'form-control', 'placeholder'=>'Retype Your password']) }}
		</div>

		{{ Form::submit('Register', ['class'=>'btn btn-default']) }}

	{{ Form::close() }}
@stop
