@extends('layouts.main')
@section('content')
	{{ Form::open(['url'=>'login-post']) }}
		<div class="form-group">
			{{ Form::label('email', 'Email')}}
			{{ Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Your email']) }}
		</div>
		<div class="form-group">
			{{ Form::label('password', 'Password')}}
			{{ Form::password('password', ['class'=>'form-control', 'placeholder'=>'Your password']) }}
		</div>

		{{ Form::submit('Login', ['class'=>'btn btn-default']) }}

	{{ Form::close() }}
@stop
