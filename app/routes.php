<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});
Route::group(array('before'=>'auth'), function() {
    Route::get('/todos/{id}/print', ['as'=>'todos.print', 'uses'=>'TodoController@printPDF']);
    Route::resource('todos', 'TodoController');
    Route::resource('todos.items', 'TodoItemController');
    Route::get('logout', 'HomeController@logout');
});

Route::get('login', 'HomeController@login');
Route::post('login-post', 'HomeController@loginPost');
Route::get('signup', 'HomeController@signup');
Route::post('signup-post', 'HomeController@signupPost');

